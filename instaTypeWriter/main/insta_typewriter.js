(function(){

var instantenaousTypeWriter = 1; // If you want default behaviour, change 1 by 0.

(function(a) {
    var b = [], c;
    GameManager.addTickListener(function(a) {
        if (0 != b.length) {
            if (isNaN(c) || 0 === c)
                c = 1;
            a *= c;
            if (0 !== a)
                for (var f = 0; f < b.length; f++)
                    b[f].tick(a, !1)
        }
    }, !1);
    a.fn.typewrite = function(d) {
        d || (d = {});
        var f = Localization.isRTLLanguage(), g = {selector: this,delay: 100,speedUpOnClick: !1,callback: null,animateScroll: !0,scrollPadding: 25,scrollPollIntervalInChars: 50};
        d && a.extend(g, d);
        f && (g.delay *= 5, g.scrollPollIntervalInChars /= 5);
        c = 1;
        var k = function(a) {
            c = (c + 2.5).clamp(1, 10)
        };
        if (d.speedUpOnClick) {
            var l = g.callback;
            g.callback = function() {
                a(window).off("click", k);
                l()
            }
        }
        for (var h = a(g.selector), m = h.text().replaceAll("<br />", "\n"), n = a("<span></span>"), p = {}, q = "", s = 0; s < m.length; s++)
            if (q += m[s], "[" == q) {
                var t = m.substr(s);
                t.startsWith("[pause:") ? (q = s + 7, s = m.indexOf("]", q), q = m.substr(q, s - q), p[n.children().length] = {type: "pause",value: parseFloat(q)}, q = "") : t.startsWith("[delay:") && (q = s + 7, s = m.indexOf("]", q), q = m.substr(q, s - q), p[n.children().length] = {type: "delay",value: q}, q = "")
            } else
                "\n" == q ? (n.append(a("<br />")), q = "") : f ? " " == 
                m[s] && (n.append(a("<span></span>").text(q)), q = "") : (n.append(a("<span></span>").text(q)), q = "");
        "" != q && n.append(a("<span></span>").text(q));
        var r = [];
        n.children().each(function() {
            a(this).css({opacity: 0})
        });
        h.empty();
        h.append(n);
        var u = 0, z = 0, w = 0, y = n.children().length, B = h.parent();
        n.children().each(function(b) {
            (function(a, b) {
                r.push(function() {
                    b.css({opacity: 1});
                    if (g.animateScroll && (w++ >= g.scrollPollIntervalInChars - 1 || a == y - 1)) {
                        w = 0;
                        0 == z && (z = h.height());
                        var c = g.scrollPadding + b.position().top - h.position().top - 
                        z;
                        0 < c && u < c && (B.stop().animate({scrollTop: c}, 600), u = c)
                    }
                })
            })(b, a(this))
        });
        var v = createjs.Tween.get(h);
        d.wait && (v = v.wait(d.wait));
        d.soundLoop && (v = v.call(function() {
            Sound.playSoundLoop(d.soundLoop, d.volume)
        }));
        f = 1;
        for (s = 0; s < r.length; s++) {
            if (p.hasOwnProperty(s))
                if ("pause" == p[s].type) {
                    v = v.call(function() {
                        d.soundLoop && Sound.stopSound(d.soundLoop)
                    }).wait(p[s].value).call(function() {
                        d.soundLoop && Sound.playSoundLoop(d.soundLoop, d.volume)
                    }).call(r[s]);
                    continue
                } else
                    "delay" == p[s].type && (f = "slow" == p[s].value ? 10 : 
                    1);
            instantenaousTypeWriter == 1 ? v = v.call(r[s]) : v = v.wait(g.delay * f ).call(r[s])
        }
        v.call(function() {
            d.soundLoop && Sound.stopSound(d.soundLoop);
            "return-tween" != d.type && -1 != b.indexOf(v) && b.remove(v);
            g.callback && g.callback()
        });
        if ("return-tween" === d.type)
            return v;
        setTimeout(function() {
            b.push(v);
            if (g.speedUpOnClick)
                a(window).on("click", k)
        }, g.delay)
    }
})(jQuery);

})();