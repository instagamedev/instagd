In order to modify the convention speed, follow the steps bellow.

1/ Go to the main folder.
2/ Open the insta_convention file with a text editor (for example notepad on windows).
3/ Locate the third line of the insta_convention file. It should look like that:

var speedInstantenaousConvention = 10;

4/ Change the 10 by another value (such as 1 if you want the default speed, such as 2.5 if you want a faster speed or such as 10 if you want an "instant convention").

If you have any question please comment on the steam decidated page.